package com.michalczop.java.spark.sql;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.date_format;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

public class SqlEx4DataFrames {

    public static void sqlEx4() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");

        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Session for SparkSQL
        SparkSession spark = SparkSession.builder()
            .appName("testingSql")
            .master("local[*]")
            .getOrCreate();

        Dataset<Row> dataset = spark.read().option("header", true).csv("src/main/resources/sql/logs/biglog.txt");

        // SQL reference
//		Dataset<Row> results = spark.sql
//		  ("select level, date_format(datetime,'MMMM') as month, count(1) as total " +
//		   "from logging_table group by level, month order by cast(first(date_format(datetime,'M')) as int), level");

        dataset = dataset.select(
            col("level"),
            date_format(col("datetime"), "MMMM").alias("month"),
            date_format(col("datetime"), "M").alias("monthnum").cast(DataTypes.IntegerType)
        );

        dataset = dataset.groupBy(col("level"), col("month"), col("monthnum")).count();
        dataset = dataset.orderBy(col("monthnum"), col("level"));
        dataset = dataset.drop(col("monthnum"));

        dataset.show(100);

        spark.close();
    }
}
