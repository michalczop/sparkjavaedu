package com.michalczop.java.spark.rdd;

import java.util.Arrays;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

public class Ex6FindMostCommonNotBoringWords {

    public static void ex6()
    {
        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // 1. Load File
        JavaRDD<String> initialRdd = sc.textFile("src/main/resources/rdd/subtitles/input.txt");
        initialRdd.take(25).forEach(System.out::println);

        System.out.println("---------------------------------");

        // 2. Trim only to words and spaces than lowercase
        JavaRDD<String> lowercaseLettersOnlyRdd = initialRdd
            .map(sentence -> sentence.replaceAll("[^a-zA-Z\\s]", "")
                .toLowerCase());
        lowercaseLettersOnlyRdd.take(25).forEach(System.out::println);

        System.out.println("---------------------------------");

        // 3. Filter blank lines
        JavaRDD<String> removedBlankLines = lowercaseLettersOnlyRdd
            .filter(sentence -> sentence.trim().length() > 0);
        removedBlankLines.take(25).forEach(System.out::println);

        System.out.println("---------------------------------");

        // 4. Map sentences to words
        JavaRDD<String> wordsRdd = removedBlankLines
            .flatMap(sentence -> Arrays.asList(sentence.split(" ")).iterator());
        wordsRdd.take(25).forEach(System.out::println);

        System.out.println("---------------------------------");

        // 5. Filter not boring words

        JavaRDD<String> notBoringWords = wordsRdd
            .filter(word -> Util.isNotBoring(word));
        notBoringWords.take(25).forEach(System.out::println);

        System.out.println("---------------------------------");

        // 6. Count
        // First map to PairRDD to be able to reduceByKey

        JavaPairRDD<String, Long> pairRDDWordLong = notBoringWords
            .mapToPair(word -> new Tuple2<String, Long>(word, 1L));
        pairRDDWordLong.take(25).forEach(System.out::println);
        /*
            (docker,1)
            (deploying,1)
            (microservice,1)
         */

        System.out.println("---------------------------------");

        // Reduce to count
        JavaPairRDD<String, Long> pairRddCount = pairRDDWordLong.reduceByKey((val1, val2) -> val1 + val2);
        pairRddCount.take(25).forEach(System.out::println);
        /*
            (webapplication,4)
            (positional,1)
            (couldve,3)
         */

        System.out.println("---------------------------------");

        // 7. Sort
        // #tip
        // JavaPairRDD can be sorted only by key so switch key with value

        JavaPairRDD<Long, String> javaRddSwitchedAndSorted =
            pairRddCount
                .mapToPair(tuple -> new Tuple2<>(tuple._2, tuple._1))
                .sortByKey(false);

        javaRddSwitchedAndSorted.take(50).forEach(System.out::println);

        System.out.println("---------------------------------");


        sc.close();
    }
}
