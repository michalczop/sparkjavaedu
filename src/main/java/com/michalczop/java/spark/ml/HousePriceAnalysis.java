package com.michalczop.java.spark.ml;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.ml.tuning.TrainValidationSplit;
import org.apache.spark.ml.tuning.TrainValidationSplitModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class HousePriceAnalysis {
	
	public static void main(String[] args) {
		
//		System.setProperty("hadoop.home.dir", "c:/hadoop");
		Logger.getLogger("org.apache").setLevel(Level.WARN);

		SparkSession spark = SparkSession.builder()
				.appName("House Price Analysis")
				.config("spark.sql.warehouse.dir","file:///c:/tmp/")
				.master("local[*]").getOrCreate();
		
		Dataset<Row> csvData = spark.read()
				.option("header", true)
				.option("inferSchema", true)
				.csv("src/main/resources/ml/kc_house_data.csv");

		csvData.printSchema();
		csvData.show();
		
		VectorAssembler vectorAssembler = new VectorAssembler()
				.setInputCols(new String[] {"bedrooms","bathrooms","sqft_living","sqft_lot","floors","grade"})
				.setOutputCol("features");
		
		Dataset<Row> modelInputData = vectorAssembler.transform(csvData)
				.select("price","features")
				.withColumnRenamed("price", "label");
		
		modelInputData.show();
		
		Dataset<Row>[] dataSplits = modelInputData.randomSplit(new double[] {0.8, 0.2});
		Dataset<Row> trainingAndTestData = dataSplits[0];
		Dataset<Row> holdOutData = dataSplits[1];

		// Setting random params of LinearRegressionModel for tests. Result is not good.
		// #url http://spark.apache.org/docs/latest/ml-classification-regression.html#linear-regression
//		LinearRegressionModel testModel = new LinearRegression()
//			.setMaxIter(10)
//			.setRegParam(0.3)
//			.setElasticNetParam(0.9)
//			.fit(trainingAndTestData);
//
//		System.out.println("The training data r2 value is (<0,1> bigger is better) " + testModel.summary().r2() + " and the RMSE is (smaller is better) " + testModel.summary().rootMeanSquaredError());
//		testModel.transform(holdOutData).show();
//		System.out.println("The test data r2 value is (<0,1> bigger is better) " + testModel.evaluate(holdOutData).r2() + " and the RMSE is (smaller is better) " + testModel.evaluate(holdOutData).rootMeanSquaredError());

		// Letting Spark to find the best params by providing him an arrays of data
		LinearRegression linearRegression = new LinearRegression();
		ParamGridBuilder paramGridBuilder = new ParamGridBuilder();
		ParamMap[] paramMap = paramGridBuilder.addGrid(linearRegression.regParam(), new double[]{0.01, 0.1, 0.5})
			.addGrid(linearRegression.elasticNetParam(), new double[]{0.0, 0.5, 1.0}).build();

		// Preparing data splitted into training and test to maximize R2.
		TrainValidationSplit trainValidationSplit = new TrainValidationSplit()
			.setEstimator(linearRegression)
			.setEvaluator(new RegressionEvaluator().setMetricName("r2"))
			.setEstimatorParamMaps(paramMap)
			.setTrainRatio(0.8);

		TrainValidationSplitModel model = trainValidationSplit.fit(trainingAndTestData);
		LinearRegressionModel lrModel = (LinearRegressionModel) model.bestModel();

		System.out.println("The training data r2 value is (<0,1> bigger is better) " + lrModel.summary().r2() + " and the RMSE is (smaller is better) " + lrModel.summary().rootMeanSquaredError());
		System.out.println("The test data r2 value is (<0,1> bigger is better) " + lrModel.evaluate(holdOutData).r2() + " and the RMSE is (smaller is better) " + lrModel.evaluate(holdOutData).rootMeanSquaredError());

		System.out.println("coeficients: " + lrModel.coefficients() + ", intercept: " + lrModel.intercept());
		System.out.println("reg param: " + lrModel.getRegParam() + ", elastic net param: " + lrModel.getElasticNetParam());

	}

}
