package com.michalczop.java.spark.sql;

import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.round;
import static org.apache.spark.sql.functions.stddev;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SqlEx7ExamResults {

    public static void sqlEx7() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkSession spark = SparkSession.builder().appName("testingSql").master("local[*]")
            .config("spark.sql.warehouse.dir","file:///c:/tmp/")
            .getOrCreate();

        Dataset<Row> dataset = spark.read().option("header", true).csv("src/main/resources/sql/exams/students.csv");

        dataset.show();

        dataset = dataset.groupBy("subject").pivot("year").agg(  round(  avg(col("score")), 2  ).alias("average") ,
                                                                 round(stddev(col("score")), 2).alias("stddev") );

        dataset.show();
    }
}
