package com.michalczop.java.spark.sql;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SqlEx2FullSqlSyntax {

    public static void sqlEx2() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");

        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Session for SparkSQL
        SparkSession spark = SparkSession.builder()
            .appName("testingSql")
            .master("local[*]")
            .getOrCreate();

        Dataset<Row> datasetStudents = spark.read().option("header", true).csv("src/main/resources/sql/exams/students.csv");
        Dataset<Row> datasetLogs = spark.read().option("header", true).csv("src/main/resources/sql/logs/biglog.txt");

        // Temporary view allows us to execute real SQL queries on dataset
        datasetStudents.createOrReplaceTempView("my_students_table");

        Dataset<Row> results = spark.sql("select distinct(year) from my_students_table order by year desc");
        results.show();

        results = spark.sql("select score, year from my_students_table where subject='French'");
        results.show();

        // date_format is a SQL function. Others can be found here:
        // http://spark.apache.org/docs/latest/api/sql/index.html
        datasetLogs.createOrReplaceTempView("logs_table");
        Dataset<Row> resultsLogs = spark.sql("select level, date_format(datetime,'MMMM') as month, date_format(datetime,'yyyy') as year, count(1) as total from logs_table group by level, year, month order by level, year, cast(first(date_format(datetime,'M')) as int) asc");

        resultsLogs.show(1000);

        spark.close();
    }
}
