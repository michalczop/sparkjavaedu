package com.michalczop.java.spark.rdd;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import scala.Tuple2;

public class Ex8Joins {

    public static void ex8()
    {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Sample data

        List<Tuple2<Integer, Integer>> visitsRaw = new ArrayList<>();
        visitsRaw.add(new Tuple2<>(4, 18));
        visitsRaw.add(new Tuple2<>(6, 7));
        visitsRaw.add(new Tuple2<>(10, 17));

        List<Tuple2<Integer, String>> namesRaw = new ArrayList<>();
        namesRaw.add(new Tuple2<>(4, "John"));
        namesRaw.add(new Tuple2<>(5, "Ben"));
        namesRaw.add(new Tuple2<>(6, "Kate"));

        JavaPairRDD<Integer, Integer> visitsPairRdd = sc.parallelizePairs(visitsRaw);
        JavaPairRDD<Integer, String> namesPairRdd = sc.parallelizePairs(namesRaw);

        // ------ (Inner) Join ------
        // #tip Spark's Join is in fact Inner Join
        // Values of Join are tuples so we have nested tuples
        // (userId, (  visits, name  ))
        // (   4  , (  18    , John  ))

        JavaPairRDD<Integer, Tuple2<Integer, String>> resultInner = visitsPairRdd.join(namesPairRdd);

        System.out.println("----- INNER JOIN -----");
        resultInner.collect().forEach(System.out::println);
        /*
            (4,(18,John))
            (6,(7,Kate))
         */

        // ------ Left Outer Joins ------
        // Left / Right joins returns Optionals, because some values might not be present
        // (userId, (  visits, name  ))
        // (   4  , (  18    , Optional[John]  ))
        // (   10 , (  17    , Optional.empty  ))

        JavaPairRDD<Integer, Tuple2<Integer, Optional<String>>> resultLeft = visitsPairRdd.leftOuterJoin(namesPairRdd);

        System.out.println("----- LEFT OUTER JOIN -----");
        resultLeft.collect().forEach(System.out::println);
        /*
            (10,(17,Optional.empty))
            (4,(18,Optional[John]))
            (6,(7,Optional[Kate]))
         */

        // ------ Right Outer Joins ------
        // (userId, (  visits, name  ))
        // (  4   , (Optional[18],John))
        // (  5   ,(Optional.empty,Ben))

        JavaPairRDD<Integer, Tuple2<Optional<Integer>, String>> resultRight = visitsPairRdd.rightOuterJoin(namesPairRdd);

        System.out.println("----- RIGHT OUTER JOIN -----");
        resultRight.collect().forEach(System.out::println);
        /*
            (4,(Optional[18],John))
            (5,(Optional.empty,Ben))
            (6,(Optional[7],Kate))
         */

        System.out.println("----- Print all user names in uppercase -----");
        resultLeft.collect().forEach(it -> System.out.println(it._2._2.orElse("no data").toUpperCase()));

        System.out.println("----- Print how many visits each user has -----");
        resultRight.collect().forEach(it -> System.out.println(it._2._2 + "has " + it._2._1.orElse(0) + " visits."));

        // ------ Full Outer Joins and Cartesians ------
        System.out.println("----- FULL OUTER JOIN -----");
        visitsPairRdd.fullOuterJoin(namesPairRdd).collect().forEach(System.out::println);
        /*
            (10,(Optional[17],Optional.empty))
            (4,(Optional[18],Optional[John]))
            (5,(Optional.empty,Optional[Ben]))
            (6,(Optional[7],Optional[Kate]))
         */

        System.out.println("----- CARTESIAN -----");
        visitsPairRdd.cartesian(namesPairRdd).collect().forEach(System.out::println);
        /*
            ((4,18),(4,John))
            ((4,18),(5,Ben))
            ((4,18),(6,Kate))
            ((6,7),(4,John))
            ((6,7),(5,Ben))
            ((6,7),(6,Kate))
            ((10,17),(4,John))
            ((10,17),(5,Ben))
            ((10,17),(6,Kate))
         */

        sc.close();
    }
}
