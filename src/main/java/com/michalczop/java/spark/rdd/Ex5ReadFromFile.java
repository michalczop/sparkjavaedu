package com.michalczop.java.spark.rdd;

import java.util.Arrays;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class Ex5ReadFromFile {

    public static void ex5()
    {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> initialRdd = sc.textFile("src/main/resources/rdd/subtitles/input.txt");

        initialRdd
            .flatMap(value -> Arrays.asList(value.split(" ")).iterator())
            .collect().forEach(System.out::println);

        sc.close();
    }
}
