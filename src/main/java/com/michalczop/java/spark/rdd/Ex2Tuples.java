package com.michalczop.java.spark.rdd;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
import scala.Tuple5;

public class Ex2Tuples {

    public static void ex2() {

        System.out.println("Ex2Tuples");

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // #tip creating tuples
        // import from scala
        // ---------- Touples ---------- //

        // example:
        Tuple5<Integer, Double, String, Integer, Integer> myTuple = new Tuple5<>(9, 3.0, "test", 1, 1);

        List<Integer> integers = new ArrayList<>();
        integers.add(4);
        integers.add(6);
        integers.add(7);
        integers.add(9);

        JavaRDD<Integer> integerJavaRdd = sc.parallelize(integers);

        // new RDD will contain integer and its sqrt
        /*
            (4,2.0)
            (6,2.449489742783178)
            (7,2.6457513110645907)
            (9,3.0)
         */
        JavaRDD<Tuple2<Integer, Double>> intAndSqrtRdd = integerJavaRdd.map(val -> new Tuple2<>(val, Math.sqrt(val)));

        intAndSqrtRdd.collect().forEach(val -> System.out.println(val));

        JavaRDD<Long> ones = intAndSqrtRdd.map(val -> 1L);
        Long count = ones.reduce((v1, v2) -> v1 + v2);
        System.out.println(count);

        sc.close();
    }
}
