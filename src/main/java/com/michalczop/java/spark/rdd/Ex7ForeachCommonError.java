package com.michalczop.java.spark.rdd;

import java.util.Arrays;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

public class Ex7ForeachCommonError {

    public static void ex7()
    {
        // #tip #foreach
        // -- Printing sorted results common error --
        // foreach call is beeing sent to each partition in parallel
        // so if we have 4 partitions with sorted data
        // println() or other lambda in foreach will be executed on them in parallel
        // causing odd order in the results.
        // Every other action than foreach will work correctly.
        // We can first take some data and than foreach them like in the example Ex6.
        // Despite the fact that the foreach will get wrong result, the data
        // on partitions are completely fine, so we DON'T have to COALESCE them.

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> initialRdd = sc.textFile("src/main/resources/rdd/subtitles/input.txt");
        JavaRDD<String> lowercaseLettersOnlyRdd = initialRdd
            .map(sentence -> sentence.replaceAll("[^a-zA-Z\\s]", "")
                .toLowerCase());
        JavaRDD<String> removedBlankLines = lowercaseLettersOnlyRdd
            .filter(sentence -> sentence.trim().length() > 0);
        JavaRDD<String> wordsRdd = removedBlankLines
            .flatMap(sentence -> Arrays.asList(sentence.split(" ")).iterator());
        JavaRDD<String> notBoringWords = wordsRdd
            .filter(word -> Util.isNotBoring(word));
        JavaPairRDD<String, Long> pairRDDWordLong = notBoringWords
            .mapToPair(word -> new Tuple2<String, Long>(word, 1L));
        JavaPairRDD<String, Long> pairRddCount = pairRDDWordLong.reduceByKey((val1, val2) -> val1 + val2);
        JavaPairRDD<Long, String> javaRddSwitchedAndSorted =
            pairRddCount
                .mapToPair(tuple -> new Tuple2<>(tuple._2, tuple._1))
                .sortByKey(false);
        javaRddSwitchedAndSorted.foreach(x -> System.out.println(x));


        sc.close();
    }
}
