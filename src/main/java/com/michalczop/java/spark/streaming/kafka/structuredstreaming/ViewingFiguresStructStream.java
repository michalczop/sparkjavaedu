package com.michalczop.java.spark.streaming.kafka.structuredstreaming;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;


public class ViewingFiguresStructStream {

    public static void main(String[] args) throws StreamingQueryException {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        Logger.getLogger("org.apache.spark.storage").setLevel(Level.ERROR);

        SparkSession session = SparkSession
            .builder()
            .master("local[*]")
            .appName("structured-streaming-report")
            .getOrCreate();

        // Config for kafka stream
        Dataset<Row> df = session
            .readStream()
            .format("kafka")
            .option("kafka.bootstrap.servers", "localhost:9092")
            .option("subscribe", "viewrecords")
            .load();

        df.createOrReplaceTempView("viewing_figures");

        // kafka sends key, value, timestamp
        // need to cast object into string - cast (value as string)
        Dataset<Row> results =
            session.sql("select window, cast (value as string) as course_name, sum(5) as seconds_watched from viewing_figures group by window(timestamp,'2 minutes'),course_name order by seconds_watched desc");
        // Using sink to log output #DataSinks
        StreamingQuery query = results
            .writeStream()
            .format("console")
            .outputMode(OutputMode.Complete())
            // to prevent truncating text in console
            .option("truncate", false)
            .option("numRows", 50)
            .start();

        query.awaitTermination();


    }
}
