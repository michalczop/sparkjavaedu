package com.michalczop.java.spark.sql;

import static org.apache.spark.sql.functions.col;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SqlEx1Filter {

    public static void sqlEx1() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");

        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Session for SparkSQL
        SparkSession spark = SparkSession.builder()
            .appName("testingSql")
            .master("local[*]")
            //.config("spark.pl.warehouse.dir","file:///c:/tmp/") //temporary directory, for Windows only
            .getOrCreate();

        Dataset<Row> dataset = spark.read().option("header", true).csv("src/main/resources/sql/exams/students.csv");

        System.out.println("no of rows: " + dataset.count());

        Row row = dataset.first();

        // get takes index on column in row as a parameter
        System.out.println("Subject in first row: " + row.get(2));

        // using header as parameter
        System.out.println("Subject in first row: " + row.getAs("subject"));

        // Filter using epression
        Dataset<Row> modernArtResults = dataset.filter("subject = 'Modern Art' AND year >= 2007 ");

        // Filter using lambda
        modernArtResults = dataset.filter(r -> r.getAs("subject").toString().equals("Modern Art")
            && Integer.parseInt(r.getAs("year")) >= 2007);

        modernArtResults = dataset.filter(col("subject").equalTo("Modern Art")
                                                           .and(col("year").geq(2007)));

        modernArtResults.show();

        spark.close();


    }
}
