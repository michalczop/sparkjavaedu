package com.michalczop.java.spark.rdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

public class Ex4FlatMapFilter {

    public static void ex4() {

        System.out.println("Ex4FlatMapFilter");

        List<String> inputData = new ArrayList<>();
        inputData.add("WARN: Tuesday 4 September 0405");
        inputData.add("ERROR: Tuesday 4 September 0408");
        inputData.add("FATAL: Wednesday 5 September 1632");
        inputData.add("ERROR: Friday 7 September 1854");
        inputData.add("WARN: Saturday 8 September 1942");

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        sc.parallelize(inputData)
            .flatMap(value -> Arrays.asList(value.split(" ")).iterator())
            .filter(word -> word.length() > 1)
            .collect().forEach(System.out::println);

        // In opposite to map, flatmap doesn't return one value for each line, but
        // can return multiple values. Here each sentence returns multiple words.
        /*
        WARN:
        Tuesday
        September
        0405
        ERROR:
        Tuesday
        September
        0408
        FATAL:
        Wednesday
        September
        1632
        ERROR:
        Friday
        September
        1854
        WARN:
        Saturday
        September
        1942
        */

        sc.close();
    }
}
