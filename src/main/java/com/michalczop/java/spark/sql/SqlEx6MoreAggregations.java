package com.michalczop.java.spark.sql;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

public class SqlEx6MoreAggregations {

    public static void sqlEx6() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession.builder()
            .appName("testingSql")
            .master("local[*]")
            .getOrCreate();

        Dataset<Row> dataset = spark.read().option("header", true).csv("src/main/resources/sql/exams/students.csv");

        dataset = dataset.groupBy("subject").agg(max(col("score").cast(DataTypes.IntegerType)).alias("max score") ,
                                                 min(col("score").cast(DataTypes.IntegerType)).alias("min score"));

        dataset.show();

        sc.close();
    }
}
