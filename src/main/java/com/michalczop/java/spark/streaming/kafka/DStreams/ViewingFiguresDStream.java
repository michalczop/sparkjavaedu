package com.michalczop.java.spark.streaming.kafka.DStreams;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;


public class ViewingFiguresDStream {

    public static void main(String[] args) throws InterruptedException {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        Logger.getLogger("org.apache.spark.storage").setLevel(Level.ERROR);

        Map<String, Object> kafkaParams = new HashMap<>();
        // List of kafka serer to connect
        kafkaParams.put("bootstrap.servers", "localhost:9092,anotherhost:9092");
        // How to convert Objects from kafka to Java objects
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        // How to convert Objects from kafka to Java objects
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        // Can be used for grouping consumers, bo not important in this case
        kafkaParams.put("group.id", "spark-group");
        // For tracking the last message we have consumed
        kafkaParams.put("auto.offset.reset", "latest");
        // https://medium.com/@danieljameskay/understanding-the-enable-auto-commit-kafka-consumer-property-12fa0ade7b65
        kafkaParams.put("enable.auto.commit", false);

        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("viewingFiguresDStreams");

        JavaStreamingContext sc = new JavaStreamingContext(conf, Durations.seconds(2));

        Collection<String> topics = Arrays.asList("viewrecords");

        JavaInputDStream<ConsumerRecord<String, String>> stream =
            KafkaUtils.createDirectStream(sc,
                                          LocationStrategies.PreferConsistent(),
                                          ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
            );

//        print to check if it works
//        JavaDStream<String> results = stream.map(item -> item.value());
//        results.print();
//        -------------------------------------------
//        Time: 1631691960000 ms
//        -------------------------------------------
//        Java Advanced Topics
//        Java Fundamentals
//        Spring Framework Fundamentals
//        Java Fundamentals
//        Hibernate and JPA
//        Hibernate and JPA
//        Java Fundamentals
//        Java Web Development Second Edition: Module 1
//        Java Advanced Topics
//        Java Advanced Topics

        // The stream shows what type of courses are beeing watcher for last 5 seconds,
        // we need to pic the most popular courses

        JavaPairDStream<Long, String> results = stream
            .mapToPair(item -> new Tuple2<>(item.value(), 5l))
            // Adding window to show results from last x seconds,
            // Slide window as second parameter causes the results to be prontes every 10s
            .reduceByKeyAndWindow((x,y) -> x+y, Durations.minutes(60), Durations.seconds(10))
            .mapToPair(item -> item.swap())
            // JavaPairDStream doesn't have sort function,
            // so we use transform function with RDD sorting as parameter
            .transformToPair(rdd -> rdd.sortByKey(false));

        results.print(50);

//        -------------------------------------------
//        Time: 1631698048000 ms
//        -------------------------------------------
//        (595,Java Fundamentals)
//        (295,Spring Framework Fundamentals)
//        (275,Hibernate and JPA)
//        (215,Java Advanced Topics)
//        (180,Spring Security Module 3)
//        (155,Docker Module 2 for Java Developers)
//        (105,Java Messaging with JMS and MDB)
//        (80,Spring JavaConfig)
//        (70,Spring MVC and WebFlow)
//        (70,Java Web Development Second Edition: Module 1)
//        (45,Groovy Programming)
//        (35,Hadoop for Java Developers)
//        (20,Java Web Development)
//        (15,Test Driven Development)
//        (10,Docker for Java Developers)
//        (5,Java Build Tools)
//        (5,NoSQL Databases)

        sc.start();
        sc.awaitTermination();

    }
}
