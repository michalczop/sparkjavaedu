package com.michalczop.java.spark.rdd;

import java.util.ArrayList;
import java.util.List;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class Ex1MapReduce {

    public static void ex1() {

        System.out.println("Ex1MapReduce");

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");

        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Prepare test data
        List<Double> doubles = new ArrayList<>();
        doubles.add(34.6);
        doubles.add(66.6);
        doubles.add(10.0);
        doubles.add(44.56);
        doubles.add(3.14);

        // #tip #parralelize
        // Loading data(collection) and turn to RDD
        // Under the hood, the JavaRDD is communicating with Scala RDD. This is a wrapper with java api.
        // works only with in memory hardcoded collections.
        JavaRDD<Double> doubleJavaRDD = sc.parallelize(doubles);

        // ---------- Reduce ---------- //
        Double result = doubleJavaRDD.reduce((value1, value2) -> value1 + value2);

        System.out.println(result);

        // ---------- Map ---------- //
        List<Integer> integers = new ArrayList<>();
        integers.add(4);
        integers.add(6);
        integers.add(7);
        integers.add(9);

        JavaRDD<Integer> integerJavaRDD = sc.parallelize(integers);

        JavaRDD<Double> sqrtRdd = integerJavaRDD.map(val -> Math.sqrt(val));

        // #tip
        // printing result - pattern only for testing!
        // println can't be serializable, so before it
        // convert RDD to collection

        sqrtRdd.collect().forEach(val -> System.out.println(val));

        // #tip
        // count RDD values for simple example
        System.out.println(sqrtRdd.count());

        // #tip
        // count using map and reduce
        // map everything to value 1
        // reduce to sum all up
        JavaRDD<Long> ones = sqrtRdd.map(val -> 1L);
        Long count = ones.reduce((v1, v2) -> v1 + v2);
        System.out.println(count);


        sc.close();
    }
}
