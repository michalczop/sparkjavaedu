package com.michalczop.java.spark.sql;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class SqlEx3InMemoryDataGroupingAndDates {

    public static void sqlEx3() {

        Logger.getLogger("org.apache").setLevel(Level.WARN);

        // Local config, without cluster - use all available cores
        SparkConf conf = new SparkConf().setAppName("startingSpark").setMaster("local[*]");

        // Connection to java spark cluster
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Session for SparkSQL
        SparkSession spark = SparkSession.builder()
            .appName("testingSql")
            .master("local[*]")
            .getOrCreate();

        List<Row> inMemory = new ArrayList<Row>();

        inMemory.add(RowFactory.create("WARN", "2016-12-31 04:19:32"));
        inMemory.add(RowFactory.create("WARN", "2016-12-30 04:19:32"));
        inMemory.add(RowFactory.create("WARN", "2017-4-30 04:19:32"));
        inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
        inMemory.add(RowFactory.create("FATAL", "2016-12-31 03:22:34"));
        inMemory.add(RowFactory.create("WARN", "2016-12-31 03:21:21"));
        inMemory.add(RowFactory.create("INFO", "2015-4-21 14:32:21"));
        inMemory.add(RowFactory.create("INFO", "2015-4-21 14:32:21"));
        inMemory.add(RowFactory.create("FATAL","2015-4-21 19:23:20"));

        StructField[] fields = new StructField[] {
            new StructField("level", DataTypes.StringType, false, Metadata.empty()),
            new StructField("datetime", DataTypes.StringType, false, Metadata.empty())
        };

        StructType schema = new StructType(fields);
        Dataset<Row> dataset = spark.createDataFrame(inMemory, schema);

        dataset.createOrReplaceTempView("in_mem_logging_table");

        // date_format is a SQL function. Others can be found here:
        // http://spark.apache.org/docs/latest/api/sql/index.html
        Dataset<Row> results = spark.sql("select level, date_format(datetime,'MMMM') as month, date_format(datetime,'yyyy') as year, count(1) as total from in_mem_logging_table group by level, year, month order by level, year, month asc");

        results.show();


        spark.close();
    }
}
